
import { Col, Row } from "reactstrap";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Button, Typography } from '@mui/material';


const OrderDetail = ({ nameProp, priceProp, img, buyProp, id }) => {

    return (
       
            <Col className="col-sm-3 p-2" style={{display: "grid", justifyContent:" space-around"}}>
                <Card sx={{ width: 150, textAlign: "center"}}>
                    <a href={"/product/" + id} >
                        <CardMedia
                            component="img"
                            height="150"
                            image={img}
                            alt="green iguana"
                        />
                    </a>

                    <CardContent>
                        <Typography gutterBottom variant="h6" component="div">
                            <b>
                                {nameProp}
                            </b>
                        </Typography>
                        <Row>
                            <Typography variant="h8" color="text.secondary">
                                <strike style={{ color: "red" }}> {buyProp}</strike> &nbsp; {priceProp}  USD
                            </Typography>
                        </Row>
                    </CardContent>
                </Card>
            </Col>
    )
}

export default OrderDetail;